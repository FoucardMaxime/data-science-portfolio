# PortFolio
My Data Scientist portfolio

Dear Reader,

This is my Data Science PortFolio gitlab. Here you will find several projects that I've made in order to:
- Learn Data Science.
- Share my journey through Data Science to anyone interested in it. My projects combine a lot of infos gathered on many different sources.
- Show what I can do.

All of my notebook are build as tutorials, as a result, it is better for me and everyone looking at it to learn the subjects covered in the notebook, and to evaluate my knowledge.

This Gitlab is structured in subjects (each directory corresponds to one specific project). It is composed of the following directories:
- COVID_project: That is the final project I have done these days about learning SKlearn, data visualisation, data cleaning and preprocessing. It is based on the work of Guillaume Saint-Cirgue from the youtube channel "Machine Learnia" https://www.youtube.com/channel/UCmpptkXu8iIFe6kfDK5o7VQ
- Keras directory that is specially dedicated to the keras projects. The main project can be seen on the "Cats_and_Dogs_Image_Classifier.ipynb" notebook, it adresses an image classification problem using convolutionnal networks and exploring how these work. The Dataset comes from a kaggle competition from 2016
- SKlearn directory is dedicated to the learning of SKlearn. It is composed of different notebooks adressing many different subjects. It shows my very first steps with Machine-Learning.
- The Tensorflow directory, that is actually composed of only one notebook that shows my first steps with tensorflow. It shows how i built some linear regression models using TensorFlow v1 and v2.
- On the main directory can be seen the source code with which i built my resume.

Enjoy !
